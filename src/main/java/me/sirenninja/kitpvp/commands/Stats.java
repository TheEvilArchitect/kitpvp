package me.sirenninja.kitpvp.commands;

import me.sirenninja.kitpvp.KitPVP;
import me.sirenninja.kitpvp.config.User;
import me.sirenninja.kitpvp.utils.Utils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Stats implements CommandExecutor {
    private KitPVP kitpvp;

    public Stats(KitPVP kitpvp){
        this.kitpvp = kitpvp;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if(!(sender instanceof Player)){
            sender.sendMessage(Utils.getColor(kitpvp.getConfig().getString("messages.mustBeAPlayer")));
            return true;
        }

        Player player = (Player)sender;
        User user = kitpvp.getPlayer(player.getUniqueId());

        player.sendMessage(Utils.getColor("&7Level: &c" + user.getLevel()));
        player.sendMessage(Utils.getColor("&7EXP: &c" + user.getEXP() + "&7/&c" + user.getMaxEXP()));
        player.sendMessage(Utils.getColor("&7Coins: &c" + user.getCoins()));
        player.sendMessage(Utils.getColor("&7Kit: &c" + (user.hasKit() ? user.getKit() : "none")));
        player.sendMessage(Utils.getColor("&7Kills: &c" + user.getKills()));
        player.sendMessage(Utils.getColor("&7Deaths: &c" + user.getDeaths()));
        player.sendMessage(Utils.getColor("&7K/D Ratio: &c" + user.getKDRatio()));

        return false;
    }
}
