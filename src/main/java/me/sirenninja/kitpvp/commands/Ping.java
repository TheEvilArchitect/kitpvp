package me.sirenninja.kitpvp.commands;

import me.sirenninja.kitpvp.KitPVP;
import me.sirenninja.kitpvp.utils.Utils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class Ping implements CommandExecutor {
    private KitPVP kitpvp;

    public Ping(KitPVP kitpvp){
        this.kitpvp = kitpvp;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if(!(sender instanceof Player)){
            sender.sendMessage(Utils.getColor(kitpvp.getConfig().getString("messages.mustBeAPlayer")));
            return true;
        }

        Player player = (Player)sender;

        int ping = ((CraftPlayer) player).getHandle().ping;
        player.sendMessage(Utils.getColor(kitpvp.getConfig().getString("messages.ping").replace("%ping%", String.valueOf(ping))));

        return false;
    }
}
