package me.sirenninja.kitpvp.commands;

import me.sirenninja.kitpvp.KitPVP;
import me.sirenninja.kitpvp.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GameModeCommand implements CommandExecutor{
    private KitPVP kitpvp;

    public GameModeCommand(KitPVP kitpvp){
        this.kitpvp = kitpvp;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if(!(sender.hasPermission("kitpvp.announcement"))){
            sender.sendMessage(Utils.getColor(kitpvp.getConfig().getString("messages.notEnoughPerms")));
            return true;
        }

        if(!(sender instanceof Player)){
            sender.sendMessage(Utils.getColor(kitpvp.getConfig().getString("messages.mustBeAPlayer")));
            return true;
        }

        Player player = (Player)sender;

        if(cmd.getName().equalsIgnoreCase("gamemode")){

            if(args.length > 0){
                switch(args[0]){

                    case ""+0:
                    case "survival":
                        applyGamemode(player, GameMode.SURVIVAL, args);
                        break;

                    case ""+1:
                    case "creative":
                        applyGamemode(player, GameMode.CREATIVE, args);
                        break;

                    case ""+2:
                    case "adventure":
                        applyGamemode(player, GameMode.ADVENTURE, args);
                        break;

                    case ""+3:
                    case "spectator":
                        applyGamemode(player, GameMode.SPECTATOR, args);
                        break;

                }
            }

        }else if(cmd.getName().equalsIgnoreCase("gms")){
            applyGamemode(player, GameMode.SURVIVAL, args);
        }else if(cmd.getName().equalsIgnoreCase("gmc")){
            applyGamemode(player, GameMode.CREATIVE, args);
        }else if(cmd.getName().equalsIgnoreCase("gma")){
            applyGamemode(player, GameMode.ADVENTURE, args);
        }else if(cmd.getName().equalsIgnoreCase("gmsp")){
            applyGamemode(player, GameMode.SPECTATOR, args);
        }

        return false;
    }

    private void applyGamemode(Player player, GameMode gamemode, String[] args){

        if(args.length > 1){
            Player target = Bukkit.getPlayer(args[1]);

            if(target == null){
                player.sendMessage(Utils.getColor(kitpvp.getConfig().getString("messages.playerNotOnline")));
                return;
            }

            target.setGameMode(gamemode);
            player.sendMessage(Utils.getColor(kitpvp.getConfig().getString("messages.gamemodeForPlayerUpdated").replace("%player%", target.getName()).replace("%gamemode%", gamemode.name())));
        }else{
            player.setGameMode(gamemode);
            player.sendMessage(Utils.getColor(kitpvp.getConfig().getString("messages.gamemodeMessage").replace("%gamemode%", gamemode.name())));
        }

    }
}
