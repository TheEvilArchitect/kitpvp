package me.sirenninja.kitpvp.commands;

import me.sirenninja.kitpvp.KitPVP;
import me.sirenninja.kitpvp.config.User;
import me.sirenninja.kitpvp.utils.KitUtils;
import me.sirenninja.kitpvp.utils.Scoreboard;
import me.sirenninja.kitpvp.utils.Utils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class Kits implements CommandExecutor, TabExecutor{
    private KitPVP kitpvp;

    public Kits(KitPVP kitpvp){
        this.kitpvp = kitpvp;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if(!(sender instanceof Player)){
            sender.sendMessage(Utils.getColor(kitpvp.getConfig().getString("messages.mustBeAPlayer")));
            return true;
        }

        Player player = (Player)sender;
        User user = kitpvp.getPlayer(player.getUniqueId());

        if(args.length > 0){
            for(String s : KitUtils.listKits()){
                if(s.equalsIgnoreCase(args[0])){

                    int cost = kitpvp.getConfig().getInt("kits." + s + ".cost", 0);
                    int level = kitpvp.getConfig().getInt("kits." + s + ".level", 1);
                    String name = kitpvp.getConfig().getString("kits." + s + ".displayname");

                    if(user.getLevel() < level) {
                        player.sendMessage(Utils.getColor(kitpvp.getConfig().getString("messages.notHighEnoughLevelForThisKit").replace("%level%", String.valueOf(level))));
                        return true;
                    }else if(!(player.hasPermission("kitpvp.kits." + s))) {
                        player.sendMessage(Utils.getColor(kitpvp.getConfig().getString("messages.notEnoughPermsForThisKit").replace("%permission%", "kitpvp.kits." + s)));
                        return true;
                    }

                    if(user.hasCooldownOnKit(s)){
                        player.sendMessage(Utils.getColor(kitpvp.getConfig().getString("messages.cooldownForThisKit").replace("%seconds%", String.valueOf((int)user.getSecondsLeftOnCooldown(s)))));
                        return true;
                    }

                    if(!(player.hasPermission("kitpvp.kits." + s))){
                        player.sendMessage(Utils.getColor(kitpvp.getConfig().getString("messages.notEnoughPermsForThisKit")));
                        return true;
                    }

                    if(!(user.hasBoughtKit(s))){
                        if(user.getCoins() < cost){
                            player.sendMessage(Utils.getColor(kitpvp.getConfig().getString("messages.notEnoughMoneyForThisKit").replace("%cost%", String.valueOf(cost))));
                            return true;
                        }

                        user.addBoughtKit(s);
                        user.removeCoins(cost);
                    }

                    if(user.hasKit())
                        player.getInventory().clear();

                    user.setKit(s);
                    KitUtils.giveKit(s, player);

                    player.sendMessage(Utils.getColor(kitpvp.getConfig().getString("messages.kitselectedMessage").replace("%kit%", name)));
                    user.setCooldown(s);

                    Scoreboard.updateScoreboard(player);
                    return true;
                }
            }

            player.sendMessage(Utils.getColor(kitpvp.getConfig().getString("messages.kitDoesNotExist")));
        }else{
            int guisize = kitpvp.getConfig().getInt("kits.gui_rows", 1);

            Inventory inv = kitpvp.getServer().createInventory(null, guisize*9, Utils.getColor("&cSelect your kit:"));

            for(String kits : KitUtils.listKits()){
                //if(!(player.hasPermission("kitpvp.kits." + kits)))
                    //continue;

                int slot = kitpvp.getConfig().getInt("kits." + kits + ".guislot", KitUtils.getNextOpenSlot(inv));
                String name = Utils.getColor(kitpvp.getConfig().getString("kits." + kits + ".displayname"));
                String item = kitpvp.getConfig().getString("kits." + kits + ".item");
                int cost = kitpvp.getConfig().getInt("kits." + kits + ".cost", 0);
                int level = kitpvp.getConfig().getInt("kits." + kits + ".level", 1);

                ItemStack itemStack = KitUtils.getItem("kits." + kits, item, name, 1);
                ItemMeta itemMeta = itemStack.getItemMeta();

                List<String> lore = itemMeta.getLore();
                List<String> replacingLore = new ArrayList<>();

                for(String s : lore){
                    replacingLore.add(s.replace("%cost%", String.valueOf(cost))
                                .replace("%permission%", "kitpvp.kits." + kits)
                                .replace("%level%", String.valueOf(level)));
                }

                itemMeta.setLore(replacingLore);
                itemStack.setItemMeta(itemMeta);

                inv.setItem(slot, itemStack);
            }

            player.openInventory(inv);

        }

        return false;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        if(!(sender instanceof Player))
            return null;

        ArrayList<String> list = new ArrayList<>();
        list.addAll(KitUtils.listKits());

        return list;
    }
}
