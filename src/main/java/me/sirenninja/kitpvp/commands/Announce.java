package me.sirenninja.kitpvp.commands;

import me.sirenninja.kitpvp.KitPVP;
import me.sirenninja.kitpvp.utils.Utils;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class Announce implements CommandExecutor{
    private KitPVP kitpvp;

    public Announce(KitPVP kitpvp){
        this.kitpvp = kitpvp;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if(!(sender.hasPermission("kitpvp.announcement"))){
            sender.sendMessage(Utils.getColor(kitpvp.getConfig().getString("messages.notEnoughPerms")));
            return true;
        }

        if(args.length > 0){

            StringBuilder builder = new StringBuilder();
            for(int i = 0; i < args.length; i++)
                builder.append(args[i]).append(" ");

            if(!(builder.toString().endsWith("-notitle"))) {
                IChatBaseComponent chatTitle = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + Utils.getColor(kitpvp.getConfig().getString("messages.announcement.title")) + "\"}");
                IChatBaseComponent chatSubtitle = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + Utils.getColor(kitpvp.getConfig().getString("messages.announcement.subtitle").replace("%announcement%", builder.toString())) + "\"}");

                PacketPlayOutTitle title = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.TITLE, chatTitle);
                PacketPlayOutTitle subtitle = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.SUBTITLE, chatSubtitle);
                PacketPlayOutTitle length = new PacketPlayOutTitle(10, (5 * 20), 10);

                for (Player players : kitpvp.getServer().getOnlinePlayers()) {
                    ((CraftPlayer) players).getHandle().playerConnection.sendPacket(title);
                    ((CraftPlayer) players).getHandle().playerConnection.sendPacket(subtitle);
                    ((CraftPlayer) players).getHandle().playerConnection.sendPacket(length);
                }

                builder.substring(0, builder.length()-7);
            }

            Bukkit.broadcastMessage(Utils.getColor(kitpvp.getConfig().getString("messages.announcement.chat").replace("%announcement%", builder.toString())));
        }

        return false;
    }
}
