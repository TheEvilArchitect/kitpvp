package me.sirenninja.kitpvp.commands;

import me.sirenninja.kitpvp.KitPVP;
import me.sirenninja.kitpvp.utils.Utils;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class Color implements CommandExecutor{
    private KitPVP kitpvp;

    public Color(KitPVP kitpvp){
        this.kitpvp = kitpvp;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if(!(sender instanceof Player)){
            sender.sendMessage(Utils.getColor(kitpvp.getConfig().getString("messages.mustBeAPlayer")));
            return true;
        }

        Player player = (Player)sender;

        Inventory inv = kitpvp.getServer().createInventory(null, 9, Utils.getColor("&cSelect your level color:"));
        inv.setItem(0, getItem("&cRED", 14));
        inv.setItem(1, getItem("&0BLACK", 15));
        inv.setItem(2, getItem("&fWHITE", 0));
        inv.setItem(3, getItem("&eYELLOW", 4));
        inv.setItem(4, getItem("&7LIGHT GRAY", 8));
        inv.setItem(5, getItem("&8DARK GRAY", 7));
        inv.setItem(6, getItem("&5PURPLE", 10));
        inv.setItem(7, getItem("&dPINK", 6));
        inv.setItem(8, getItem("&2LIME GREEN", 5));
        player.openInventory(inv);

        return false;
    }

    private ItemStack getItem(String displayname, int id){
        ItemStack item = new ItemStack(Material.WOOL.getId(), 1, (byte)id);

        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(Utils.getColor(displayname));
        meta.setLore(Arrays.asList(Utils.getColor("&3Click on one of these wool colors"), Utils.getColor("&3to change your level color in chat!")));

        item.setItemMeta(meta);

        return item;
    }
}
