package me.sirenninja.kitpvp.events;

import me.sirenninja.kitpvp.KitPVP;
import me.sirenninja.kitpvp.config.User;
import me.sirenninja.kitpvp.utils.KitUtils;
import me.sirenninja.kitpvp.utils.Scoreboard;
import me.sirenninja.kitpvp.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.UUID;

public class PlayerEvents implements Listener {
    private KitPVP kitpvp;

    public PlayerEvents(KitPVP kitpvp){
        this.kitpvp = kitpvp;
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event){
        Player player = event.getPlayer();

        kitpvp.addPlayer(player.getUniqueId());

        for(Player p : KitPVP.getInstance().getServer().getOnlinePlayers())
            Scoreboard.updateScoreboard(p);

        event.setJoinMessage(null);
        event.setJoinMessage(Utils.getColor(kitpvp.getConfig().getString("messages.joinMessage")
                                                                .replace("%player%", player.getName())));
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event){
        Player player = event.getPlayer();
        User user = kitpvp.getPlayer(player.getUniqueId());

        if (user.isCombatTagged()){
            player.setHealth(0);

            for (ItemStack item : player.getInventory().getContents()) {
                if (item != null)
                    player.getLocation().getWorld().dropItemNaturally(player.getLocation(), item.clone()).setPickupDelay(20);

            }
            player.getInventory().clear();

            User killer = kitpvp.getPlayer(user.getCombatLogger());
            killer.addKills(1);
            killer.removeCombatTag();

            user.removeCombatTag();

            Bukkit.broadcastMessage(Utils.getColor(kitpvp.getConfig().getString("messages.playerCombatLogged").replace("%player%", player.getName())));
        }

        event.setQuitMessage(null);
        event.setQuitMessage(Utils.getColor(kitpvp.getConfig().getString("messages.quitMessage")
                                                                .replace("%player%", player.getName())));

        for(Player p : kitpvp.getServer().getOnlinePlayers()) {
            if(p != player)
                Scoreboard.updateScoreboard(p);
        }

        new BukkitRunnable(){

            @Override
            public void run(){
                Player p = Bukkit.getPlayer(player.getUniqueId());

                if(p == null)
                    kitpvp.removePlayer(player.getUniqueId());
            }

        }.runTaskLaterAsynchronously(kitpvp, 300*20);
    }

    @EventHandler
    public void onPlayerDied(PlayerDeathEvent event){
        Player player = event.getEntity();
        Player killer = event.getEntity().getKiller();

        User user = kitpvp.getPlayer(player.getUniqueId());
        User kUser = kitpvp.getPlayer(killer.getUniqueId());

        user.addDeaths(1);
        user.removeCombatTag();
        user.setKit(null);
        kUser.addKills(1);
        kUser.removeCombatTag();

        Scoreboard.updateScoreboard(killer);

        event.setDeathMessage(Utils.getColor(kitpvp.getConfig().getString("messages.deathMessage")
                                                                .replace("%killer%", killer.getName())
                                                                .replace("%player%", player.getName())));
    }

    @EventHandler
    public void onRespawn(PlayerRespawnEvent event){
        Scoreboard.updateScoreboard(event.getPlayer());
    }

    @EventHandler
    public void onAttack(EntityDamageByEntityEvent event){
        if(!(event.getEntity() instanceof Player) || !(event.getDamager() instanceof Player))
            return;

        Player player = (Player)event.getEntity();
        Player damager = (Player)event.getDamager();

        User user = kitpvp.getPlayer(player.getUniqueId());
        User kUser = kitpvp.getPlayer(damager.getUniqueId());

        user.setCombatTagged(damager.getUniqueId());
        kUser.setCombatTagged(player.getUniqueId());
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onChat(AsyncPlayerChatEvent event){
        Player player = event.getPlayer();
        User user = kitpvp.getPlayer(player.getUniqueId());

        String prefix = Utils.getColor(kitpvp.getConfig().getString("messages.levelprefix").replace("%level%", "&" + user.getLevelColor() + user.getLevel() + "&r"));

        event.setFormat(prefix + event.getFormat());
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event){
        Player player = (Player)event.getWhoClicked();
        User user = kitpvp.getPlayer(player.getUniqueId());

        if(event.getInventory().getTitle().equalsIgnoreCase(Utils.getColor("&cSelect your kit:"))){
            event.setCancelled(true);
            if(event.getCurrentItem() == null)
                return;

            for(String kits : KitUtils.listKits()){
                int slot = kitpvp.getConfig().getInt("kits." + kits + ".guislot");
                int cost = kitpvp.getConfig().getInt("kits." + kits + ".cost", 0);
                int level = kitpvp.getConfig().getInt("kits." + kits + ".level", 1);

                if(event.getSlot() == slot){

                    if(user.getLevel() < level) {
                        player.sendMessage(Utils.getColor(kitpvp.getConfig().getString("messages.notHighEnoughLevelForThisKit").replace("%level%", String.valueOf(level))));
                        return;
                    }else if(!(player.hasPermission("kitpvp.kits." + kits))) {
                        player.sendMessage(Utils.getColor(kitpvp.getConfig().getString("messages.notEnoughPermsForThisKit").replace("%permission%", "kitpvp.kits." + kits)));
                        return;
                    }

                    if(user.hasCooldownOnKit(kits)){
                        player.sendMessage(Utils.getColor(kitpvp.getConfig().getString("messages.cooldownForThisKit").replace("%seconds%", String.valueOf((int)user.getSecondsLeftOnCooldown(kits)))));
                        return;
                    }

                    if(user.getLevel() >= level){
                        if(!(user.hasBoughtKit(kits))){
                            if(user.getCoins() < cost){
                                player.sendMessage(Utils.getColor(kitpvp.getConfig().getString("messages.notEnoughMoneyForThisKit").replace("%cost%", String.valueOf(cost))));
                                return;
                            }

                            user.addBoughtKit(kits);
                            user.removeCoins(cost);
                        }

                        if(user.hasKit())
                            player.getInventory().clear();

                        user.setKit(kits);
                        KitUtils.giveKit(kits, player);

                        String name;
                        if(event.getCurrentItem().hasItemMeta()) {
                            ItemMeta im = event.getCurrentItem().getItemMeta();
                            name = im.getDisplayName();
                        }else
                            name = "&cNo displayname!";

                        player.sendMessage(Utils.getColor(kitpvp.getConfig().getString("messages.kitselectedMessage").replace("%kit%", name)));

                        Scoreboard.updateScoreboard(player);
                        player.closeInventory();
                        return;
                    }
                }
            }

            return;
        }

        if(event.getInventory().getTitle().equalsIgnoreCase(Utils.getColor("&cSelect your level color:"))){
            event.setCancelled(true);
            if(event.getCurrentItem() == null)
                return;

            switch(event.getSlot()){
                case 0:
                    user.setLevelColor("c");
                    player.closeInventory();
                    break;
                case 1:
                    user.setLevelColor("0");
                    player.closeInventory();
                    break;
                case 2:
                    user.setLevelColor("f");
                    player.closeInventory();
                    break;
                case 3:
                    user.setLevelColor("e");
                    player.closeInventory();
                    break;
                case 4:
                    user.setLevelColor("7");
                    player.closeInventory();
                    break;
                case 5:
                    user.setLevelColor("8");
                    player.closeInventory();
                    break;
                case 6:
                    user.setLevelColor("5");
                    player.closeInventory();
                    break;
                case 7:
                    user.setLevelColor("d");
                    player.closeInventory();
                    break;
                case 8:
                    user.setLevelColor("2");
                    player.closeInventory();
                    break;
            }
        }
    }
}
