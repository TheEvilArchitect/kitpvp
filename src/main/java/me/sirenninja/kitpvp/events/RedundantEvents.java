package me.sirenninja.kitpvp.events;

import me.sirenninja.kitpvp.KitPVP;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.ItemStack;

public class RedundantEvents implements Listener {
    private KitPVP kitpvp;

    public RedundantEvents(KitPVP kitpvp){
        this.kitpvp = kitpvp;
    }

    @EventHandler
    public void placeBlock(BlockPlaceEvent event){
        if(kitpvp.getConfig().getBoolean("booleans.disableBlockPlace"))
            event.setCancelled(true);
    }

    @EventHandler
    public void blockBreak(BlockBreakEvent event){
        if(kitpvp.getConfig().getBoolean("booleans.disableBlockBreak"))
            event.setCancelled(true);
    }

    @EventHandler
    public void foodChangeEvent(FoodLevelChangeEvent event){
        if(kitpvp.getConfig().getBoolean("booleans.disableHunger")) {
            event.setFoodLevel(20);
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onItemDrop(PlayerDropItemEvent event) {
        if(kitpvp.getConfig().getBoolean("booleans.disableItemDrop"))
            event.setCancelled(true);
    }

    @EventHandler
    public void onPlayerCraft(PrepareItemCraftEvent event) {
        if(kitpvp.getConfig().getBoolean("booleans.disableItemCrafting"))
            event.getInventory().setResult(new ItemStack(Material.AIR));
    }

    @EventHandler
    public void onBucketEmpty(PlayerBucketEmptyEvent event){
        if(kitpvp.getConfig().getBoolean("booleans.disableWaterPlacement")){
            if(event.getBucket() == Material.WATER_BUCKET)
                event.setCancelled(true);
        }

        if(kitpvp.getConfig().getBoolean("booleans.disableLavaPlacement")){
            if(event.getBucket() == Material.LAVA_BUCKET)
                event.setCancelled(true);
        }
    }
}
