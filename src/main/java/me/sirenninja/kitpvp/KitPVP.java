package me.sirenninja.kitpvp;

import me.sirenninja.kitpvp.commands.*;
import me.sirenninja.kitpvp.config.User;
import me.sirenninja.kitpvp.events.PlayerEvents;
import me.sirenninja.kitpvp.events.RedundantEvents;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.UUID;

public class KitPVP extends JavaPlugin {

    private static KitPVP instance;

    private HashMap<UUID, User> players = new HashMap<>();

    /**
     * TODO (UNFINISHED):
     *
     *
     *
     */

    /**
     * TODO (FINISHED):
     * Stats.
     * Leveling/EXP.
     * Combat log.
     * Coins.
     * Scoreboard.
     * Kill streaks.
     * Add the level to chat.
     * /level color or /color to change their level color.
     * Kits with required levels, perms, and coins.
     * Sender not a player message.
     * Ping message.
     * Announcement command.
     * Gamemode/gms/gmc/gma/gmsp commands.
     */


    @Override
    public void onEnable(){
        instance = this;

        getConfig().options().copyDefaults(true);
        saveConfig();
        reloadConfig();

        getCommand("stats").setExecutor(new Stats(this));
        getCommand("color").setExecutor(new Color(this));
        getCommand("ping").setExecutor(new Ping(this));
        getCommand("gamemode").setExecutor(new GameModeCommand(this));
        getCommand("gms").setExecutor(new GameModeCommand(this));
        getCommand("gmc").setExecutor(new GameModeCommand(this));
        getCommand("gma").setExecutor(new GameModeCommand(this));
        getCommand("gmsp").setExecutor(new GameModeCommand(this));
        getCommand("announce").setExecutor(new Announce(this));

        getCommand("kits").setExecutor(new Kits(this));
        getCommand("kits").setTabCompleter(new Kits(this));

        getServer().getPluginManager().registerEvents(new PlayerEvents(this), this);
        getServer().getPluginManager().registerEvents(new RedundantEvents(this), this);

        for(Player players : getServer().getOnlinePlayers())
            addPlayer(players.getUniqueId());
    }

    @Override
    public void onDisable(){
        instance = null;
    }

    public void addPlayer(UUID uuid){
        players.putIfAbsent(uuid, new User(this, uuid));
    }

    public User getPlayer(UUID uuid){
        if(!(players.containsKey(uuid)))
            addPlayer(uuid);

        return players.get(uuid);
    }

    public void removePlayer(UUID uuid){
        players.remove(uuid);
    }

    public static KitPVP getInstance(){
        return instance;
    }
}
