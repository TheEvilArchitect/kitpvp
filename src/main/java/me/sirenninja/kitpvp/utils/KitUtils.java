package me.sirenninja.kitpvp.utils;

import me.sirenninja.kitpvp.KitPVP;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class KitUtils {

    public static ArrayList<String> listKits(){
        ArrayList<String> kits = new ArrayList<>();
        kits.addAll(KitPVP.getInstance().getConfig().getConfigurationSection("kits").getKeys(false));

        return kits;
    }

    public static ArrayList<String> listItems(String kit){
        ArrayList<String> items = new ArrayList<>();
        items.addAll(KitPVP.getInstance().getConfig().getConfigurationSection("kits." + kit + ".items").getKeys(false));

        return items;
    }

    public static void giveKit(String kit, Player player){
        setArmor("kits." + kit, player);

        for(String kititems : listItems(kit)){
            if(!(Utils.isNumber(kititems)))
                return;

            int slot = Integer.parseInt(kititems);
            int amount = KitPVP.getInstance().getConfig().getInt("kits." + kit + ".items." + slot + ".amount", 1);
            String name = Utils.getColor(KitPVP.getInstance().getConfig().getString("kits." + kit + ".items." + slot + ".displayname"));
            String item = KitPVP.getInstance().getConfig().getString("kits." + kit + ".items." + slot + ".item");

            player.getInventory().setItem(slot, getItem("kits." + kit + ".items." + slot, item, name, amount));
        }
    }

    private static void setArmor(String path, Player player){
        if(KitPVP.getInstance().getConfig().isSet(path + ".armor.head") && isMaterial(KitPVP.getInstance().getConfig().getString(path + ".armor.head")))
            player.getInventory().setHelmet(new ItemStack(Material.getMaterial(KitPVP.getInstance().getConfig().getString(path + ".armor.head").toUpperCase())));

        if(KitPVP.getInstance().getConfig().isSet(path + ".armor.chestplate") && isMaterial(KitPVP.getInstance().getConfig().getString(path + ".armor.chestplate")))
            player.getInventory().setChestplate(new ItemStack(Material.getMaterial(KitPVP.getInstance().getConfig().getString(path + ".armor.chestplate").toUpperCase())));

        if(KitPVP.getInstance().getConfig().isSet(path + ".armor.leggings") && isMaterial(KitPVP.getInstance().getConfig().getString(path + ".armor.leggings")))
            player.getInventory().setLeggings(new ItemStack(Material.getMaterial(KitPVP.getInstance().getConfig().getString(path + ".armor.leggings").toUpperCase())));

        if(KitPVP.getInstance().getConfig().isSet(path + ".armor.boots") && isMaterial(KitPVP.getInstance().getConfig().getString(path + ".armor.boots")))
            player.getInventory().setBoots(new ItemStack(Material.getMaterial(KitPVP.getInstance().getConfig().getString(path + ".armor.boots").toUpperCase())));
    }

    public static int getNextOpenSlot(Inventory inv){
        int i = 1;
        for(ItemStack item : inv.getContents()){
            if(item == null)
                return i;

            i++;
        }

        return 0;
    }

    private static boolean isMaterial(String material){
        return Material.getMaterial(material.toUpperCase()) != null;
    }

    public static ItemStack getItem(String item, String type, String name, int amount){
        ItemStack i = new ItemStack(Material.getMaterial(type.toUpperCase()), amount);
        ItemMeta meta = i.getItemMeta();

        if(!(name == null))
            meta.setDisplayName(name);

        if(KitPVP.getInstance().getConfig().isSet(item + ".lore")){
            ArrayList<String> lore = new ArrayList<>();

            for(String lores : KitPVP.getInstance().getConfig().getStringList(item + ".lore"))
                lore.add(Utils.getColor(lores));

            meta.setLore(lore);
        }

        if(KitPVP.getInstance().getConfig().isSet(item + ".enchantments")){
            for(String s : KitPVP.getInstance().getConfig().getStringList(item + ".enchantments")) {
                String[] en = s.split(":");
                meta.addEnchant(Enchantment.getByName(en[0].toUpperCase()), (Utils.isNumber(en[1]) ? Integer.parseInt(en[1]) : 1), true);
            }
        }

        i.setItemMeta(meta);
        return i;
    }
}
