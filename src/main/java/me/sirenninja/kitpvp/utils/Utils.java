package me.sirenninja.kitpvp.utils;

import org.bukkit.ChatColor;

public class Utils {

    public static String getColor(String message){
        return ChatColor.translateAlternateColorCodes('&', message);
    }

    public static boolean isNumber(String s){
        return s.matches("\\d+");
    }

}
