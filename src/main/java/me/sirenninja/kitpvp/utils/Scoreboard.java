package me.sirenninja.kitpvp.utils;

import me.sirenninja.kitpvp.KitPVP;
import me.sirenninja.kitpvp.config.User;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;

import java.util.ArrayList;
import java.util.List;

public class Scoreboard {

    public static void updateScoreboard(Player player){
        new BukkitRunnable(){

            @Override
            public void run(){
                    if(player != null)
                        scoreboard(player);
            }

        }.runTaskLater(KitPVP.getInstance(), 5);
    }

    private static void scoreboard(Player player){
        if(player == null)
            return;

        User user = KitPVP.getInstance().getPlayer(player.getUniqueId());

        org.bukkit.scoreboard.Scoreboard board = KitPVP.getInstance().getServer().getScoreboardManager().getNewScoreboard();
        Objective objective = board.registerNewObjective("Scoreboard", "dummy");
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);

        objective.setDisplayName(replacer(user, player.getName(), KitPVP.getInstance().getConfig().getString("scoreboard.header")));

        List<String> lines = new ArrayList<>();
        for(String s : KitPVP.getInstance().getConfig().getStringList("scoreboard.scores"))
            lines.add(replacer(user, player.getName(), s));

        for (int i = 0; i < lines.size(); i++){
            Score score = objective.getScore(lines.get(i) + addResets(i));
            score.setScore(lines.size() - i);
        }

        player.setScoreboard(board);
    }

    private static String replacer(User user, String playerName, String message){
        return Utils.getColor(message)
                .replace("%level%", String.valueOf(user.getLevel()))
                .replace("%exp%", String.valueOf(user.getEXP()))
                .replace("%maxexp%", String.valueOf(user.getMaxEXP()))
                .replace("%kills%", String.valueOf(user.getKills()))
                .replace("%deaths%", String.valueOf(user.getDeaths()))
                .replace("%kdratio%", String.valueOf(user.getKDRatio()))
                .replace("%online%", String.valueOf(KitPVP.getInstance().getServer().getOnlinePlayers().size()))
                .replace("%username%", playerName)
                .replace("%coins%", String.valueOf(user.getCoins()));
    }

    private static String addResets(int i){
        StringBuilder sb = new StringBuilder();
        for(int j = 0; j < i; j++)
            sb.append("&r");

        return Utils.getColor(sb.toString());
    }

}
