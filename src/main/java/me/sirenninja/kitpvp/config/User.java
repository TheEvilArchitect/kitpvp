package me.sirenninja.kitpvp.config;

import me.sirenninja.kitpvp.KitPVP;
import me.sirenninja.kitpvp.config.player.PlayerConfig;
import me.sirenninja.kitpvp.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.io.File;
import java.text.DecimalFormat;
import java.util.*;

public class User {
    private KitPVP kitpvp;

    private HashMap<String, Long> cooldownKits = new HashMap<>();

    private String kit;

    private UUID playerUUID;
    private PlayerConfig config;

    private boolean isCombatLogged = false;
    private int combatLimit = 30;
    private UUID combatLogger;

    private int killstreak = 0;
    private boolean hasKillstreak = false;

    private BukkitTask br;

    public User(KitPVP kitpvp, UUID playerUUID){
        this.kitpvp = kitpvp;
        this.playerUUID = playerUUID;

        config = new PlayerConfig(playerUUID, new File(KitPVP.getInstance().getDataFolder(), "data" + File.separator));
    }

    public boolean hasKit(){
        return kit != null;
    }

    public void setKit(String kit){
        this.kit = kit;
    }

    public String getKit(){
        return kit;
    }

    public boolean hasBoughtKit(String kit){
        return config.boughtKits().contains(kit);
    }

    public void addBoughtKit(String kit){
        config.addKit(kit);
    }

    public String getLevelColor(){
        return config.getColor();
    }

    public void setLevelColor(String color){
        config.setColor(color);

        Bukkit.getPlayer(playerUUID).sendMessage(Utils.getColor("&" + color + "Your color has been changed!"));
    }

    public int getLevel(){
        return config.getLevel();
    }

    private void addLevel(int level){
        config.addLevel(level);
    }

    public int getEXP(){
        return config.getEXP();
    }

    private void addEXP(int exp){
        if(levelUp(getEXP()+exp)){
            Bukkit.broadcastMessage(Utils.getColor(kitpvp.getConfig().getString("messages.levelUp")
                                                                        .replace("%player%", Bukkit.getOfflinePlayer(playerUUID).getName())
                                                                        .replace("%level%", String.valueOf(getLevel()))));
            return;
        }

        config.addEXP(exp);
    }

    public int getKills(){
        return config.getKills();
    }

    public void addKills(int kills){
        config.addKills(kills);

        int exp = kitpvp.getConfig().getInt("stats.kills.addEXP");
        int coins = kitpvp.getConfig().getInt("stats.kills.addCoins");

        addEXP(exp);
        addCoins(coins);

        Bukkit.getPlayer(playerUUID).sendMessage(Utils.getColor(kitpvp.getConfig().getString("messages.expAndCoinMessage")
                                                                                    .replace("%coins%", String.valueOf(coins))
                                                                                    .replace("%exp%", String.valueOf(exp))));

        killstreak++;

        for(String i : KitPVP.getInstance().getConfig().getConfigurationSection("killstreaks").getKeys(false)){
            if(!(Utils.isNumber(i)))
                continue;

            int k = Integer.parseInt(i);

            if(killstreak == k){
                kitpvp.getServer().broadcastMessage(Utils.getColor(kitpvp.getConfig().getString("killstreaks." + i + ".message")
                                                                                  .replace("%player%", kitpvp.getServer().getOfflinePlayer(playerUUID).getName())));

                hasKillstreak = true;
                return;
            }
        }
    }

    public int getDeaths(){
        return config.getDeaths();
    }

    public void addDeaths(int deaths){
        config.addDeaths(deaths);

        if(hasKillstreak){
            kitpvp.getServer().broadcastMessage(Utils.getColor(kitpvp.getConfig().getString("killstreaks.killstreakEnded")
                    .replace("%player%", kitpvp.getServer().getOfflinePlayer(playerUUID).getName())));
        }

        killstreak = 0;
        hasKillstreak = false;
    }

    public int getCoins(){
        return config.getCoins();
    }

    public void addCoins(int coins){
        config.addCoins(coins);
    }

    public void removeCoins(int coins){
        config.removeCoins(coins);
    }

    public double getKDRatio(){
        if(getKills() > 0 && getDeaths() == 0)
            return getKills();

        if(getKills() > 0)
            return Double.parseDouble(new DecimalFormat("##.##").format(getKills()/getDeaths()));

        return 0;
    }

    public boolean isCombatTagged(){
        return isCombatLogged;
    }

    public UUID getCombatLogger(){
        return combatLogger;
    }

    public void removeCombatTag(){
        combatLogger = null;
        combatLimit = 0;
        isCombatLogged = false;
        br.cancel();
        br = null;

        try{
            Bukkit.getPlayer(playerUUID).sendMessage(Utils.getColor(kitpvp.getConfig().getString("messages.combatTagRemoved")));
        }catch(Exception e){
            // Screw this exception.
        }
    }

    public void setCombatTagged(UUID targetUUID){
        if(br != null && combatLogger != null){
            combatLimit = 30;
            return;
        }

        combatLogger = targetUUID;
        Bukkit.getPlayer(playerUUID).sendMessage(Utils.getColor(kitpvp.getConfig().getString("messages.combatTagged")));
        combatLimit = 30;
        isCombatLogged = true;

        br = new BukkitRunnable(){

            @Override
            public void run(){
                if(combatLimit <= 0){
                   combatLogger = null;
                   combatLimit = 0;
                   isCombatLogged = false;

                   br = null;

                   try{
                       Bukkit.getPlayer(playerUUID).sendMessage(Utils.getColor(kitpvp.getConfig().getString("messages.combatTagRemoved")));
                   }catch(Exception e){
                       // Screw this exception.
                   }

                   this.cancel();
                   return;
                }

                combatLimit--;
            }

        }.runTaskTimerAsynchronously(kitpvp, 0, 20);
    }

    public void setCooldown(String kit){
        if(hasCooldownOnKit(kit))
            return;

        cooldownKits.putIfAbsent(kit, System.currentTimeMillis());
        int seconds = kitpvp.getConfig().getInt("kits." + kit + ".cooldown_in_seconds", 1);

        new BukkitRunnable(){

            @Override
            public void run(){
                cooldownKits.remove(kit);

                if(cooldownKits.isEmpty()) {
                    this.cancel();
                }
            }

        }.runTaskLaterAsynchronously(kitpvp, seconds*20);
    }

    public boolean hasCooldownOnKit(String kit){
        return cooldownKits.containsKey(kit);
    }

    public long getSecondsLeftOnCooldown(String kit){
        if(hasCooldownOnKit(kit)){
            int seconds = kitpvp.getConfig().getInt("kits." + kit + ".cooldown_in_seconds", 1);
            return ((cooldownKits.get(kit)/1000) + seconds) - System.currentTimeMillis()/1000;
        }

        return 0;
    }

    public int getMaxEXP(){
        return (getLevel() * 520);
    }

    private boolean levelUp(int exp){
        if(exp >= getMaxEXP()){
            config.resetEXP();
            addLevel(1);

            return true;
        }

        return false;
    }
}
