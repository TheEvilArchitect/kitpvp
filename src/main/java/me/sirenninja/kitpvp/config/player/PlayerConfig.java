package me.sirenninja.kitpvp.config.player;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class PlayerConfig {

    private File file;

    private FileConfiguration config;

    public PlayerConfig(UUID playerUUID, File root){
        this.file = new File(root, playerUUID.toString() + ".yml");

        if(!(root.exists()))
            root.mkdirs();

        try{
            if(!(file.exists()))
                file.createNewFile();

            config = YamlConfiguration.loadConfiguration(file);

            config.set("PlayerData.username", Bukkit.getPlayer(playerUUID).getName());
            addDefault("PlayerData.uuid", playerUUID.toString());
            addDefault("PlayerData.levelColor", "c");
            addDefault("PlayerData.stats.level", 1);
            addDefault("PlayerData.stats.exp", 0);
            addDefault("PlayerData.stats.kills", 0);
            addDefault("PlayerData.stats.deaths", 0);
            addDefault("PlayerData.kits", new ArrayList<String>());

            save();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void setColor(String color){
        config.set("PlayerData.levelColor", color);
        save();
    }

    public String getColor(){
        return config.getString("PlayerData.levelColor");
    }

    public List<String> boughtKits(){
        return config.getStringList("PlayerData.kits");
    }

    public void addKit(String kit){
        List<String> kits = boughtKits();

        if(kits.contains(kit))
            return;

        kits.add(kit);

        config.set("PlayerData.kits", kits);
        save();
    }

    public int getLevel(){
        return config.getInt("PlayerData.stats.level");
    }

    public void addLevel(int level){
        config.set("PlayerData.stats.level", (getLevel()+level));
        save();
    }

    public void resetLevel(){
        config.set("PlayerData.stats.level", 1);
        save();
    }

    public int getEXP(){
        return config.getInt("PlayerData.stats.exp");
    }

    public void addEXP(int exp){
        config.set("PlayerData.stats.exp", (getEXP()+exp));
        save();
    }

    public void resetEXP(){
        config.set("PlayerData.stats.exp", 0);
        save();
    }

    public int getKills(){
        return config.getInt("PlayerData.stats.kills");
    }

    public void addKills(int kills){
        config.set("PlayerData.stats.kills", (getKills()+kills));
        save();
    }

    public void resetKills(){
        config.set("PlayerData.stats.kills", 0);
        save();
    }

    public int getDeaths(){
        return config.getInt("PlayerData.stats.deaths");
    }

    public void addDeaths(int deaths){
        config.set("PlayerData.stats.deaths", (getDeaths()+deaths));
        save();
    }

    public void resetDeaths(){
        config.set("PlayerData.stats.deaths", 0);
        save();
    }

    public int getCoins(){
        return config.getInt("PlayerData.stats.coins");
    }

    public void addCoins(int coins){
        config.set("PlayerData.stats.coins", (getCoins()+coins));
        save();
    }

    public void removeCoins(int coins){
        config.set("PlayerData.stats.coins", (getCoins()-coins));
        save();
    }

    public void resetCoins(){
        config.set("PlayerData.stats.coins", 0);
        save();
    }

    private void save(){
        try{
            config.save(file);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void addDefault(String path, Object value){
        if(!(config.isSet(path)))
            config.set(path, value);
    }

}
